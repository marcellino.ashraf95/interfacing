#include "STM32F103Cx.h"
#include "driver.h"

void SystemInit(){};

int main()
{
	//Setup
	portInit(GPIOA);
	portInit(GPIOB);
	USART1_Init();
	Robot_Start();
	GPIOA->CRL |= 0x33333333;
	GPIOA->CRH |= 0x33333333;
	GPIOB->CRL |= 0x44444444;
	GPIOB->CRH |= 0x44444444;
	//Loop
	while (1)
	{
		//if (GPIOB->ODR & 0x0001)
			Robot_Forward();
	}
}
