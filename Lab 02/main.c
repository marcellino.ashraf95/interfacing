#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	int counter = 0, i = 0;

	//Setup
	RCC->APB2ENR |= (1<<4);	//CLK to PORTC (input)
	RCC->APB2ENR |= (1<<2);	//CLK to PORTA (output)
	
	GPIOA->CRH = 0x40000000;	//Pin A15 input
	GPIOC->CRH = 0x33000000;	//Pins C14 LED & C15 Buzzer output
	
	
	//Loop
	while (1)
	{
		if (GPIOA->IDR & 0x8000)
		{
			counter++;
			for (i = 0; i < 10000000; i++);
		}
		if (counter == 10)
		{
			GPIOC->ODR = 0;
			GPIOC->ODR |= (1<<15);
			for (i = 0; i < 10000000; i++);
			counter = 0;
		}
		else
		{
			GPIOC->ODR = 0;
			GPIOC->ODR |= (1<<14);
			for (i = 0; i < 1000000; i++);
		}
			
	}
	
	
	return 0;
}
