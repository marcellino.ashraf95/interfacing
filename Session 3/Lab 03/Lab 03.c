#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	int i;
	//Enable clock for GPIOA from APB2ENR RCC
	RCC->APB2ENR |= (1<<2);
	//Enable clock for GPIOC from APB2ENR RCC
	RCC->APB2ENR |= (1<<4);
	
	//Configuration [one time]
	GPIOA->CRL = 0x00000004; //PA0 input float
	GPIOC->CRH = 0x00300000; //PC13 output 50MHz (led)
	
	//LOOP
	while(1)
	{
		if(GPIOA->IDR & 0x0001)
		{
			GPIOC->ODR &= ~(1<<13);
			for (i = 0; i < 100000; i++);
		}
		else
		{
			GPIOC->ODR |= (1<<13);
			for (i = 0; i < 100000; i++);
		}
	}
	
	
	
	
	return 0;
}
