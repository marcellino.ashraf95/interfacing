#include <stm32f10x.h>	//include the addresses of registers from the microcontroller header file
#include <stdio.h>

int main()
{
	int i;
	//Enable clock for GPIOC from APB2ENR RCC (reset clock and cont
	RCC->APB2ENR |= (1<<4);
	
	//Configuration [one time]
	//PinC13 to be output with high speed [3] 0x40011004
	GPIOC->CRH = 0x00300000;
	
	//LOOP
	while(1)
	{
		GPIOC->ODR |= (1<<13);	 //Set PinC13 = 1
		for(i=0; i<1000000; i++); //delay
		GPIOC->ODR &= ~(1<<13);	 //Set PinC13 = 0
		for(i=0; i<1000000; i++); //delay
	}
	
	return 0;
}
