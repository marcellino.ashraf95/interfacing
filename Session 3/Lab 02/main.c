#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	int i;
	//Enable clock for GPIOA from APB2ENR RCC
	RCC->APB2ENR |= (1<<2);
	
	//Configuration [one time]
	GPIOA->CRH = 0x33333333;
	GPIOA->CRL = 0x33333333;
	
	//LOOP
	while(1)
	{
		GPIOA->ODR &= 0;	 //Reset all pins to 0
		for(i=0; i<100000; i++); //delay
		GPIOA->ODR |= 0xFFFF;	 //Set all pins to 1
		for(i=0; i<100000; i++); //delay
	}
	
	
	
	
	return 0;
}
