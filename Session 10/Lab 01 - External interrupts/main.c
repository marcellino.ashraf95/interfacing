#include "STM32F103Cx.h"

void SystemInit(){};
	
		int delay = 0;
	
int main()
{
	//Setup
	portInit(GPIOA);						//Initialize PORTA
	GPIOA->CRL |= 0x00000004;		//PA0 INPUT
	RCC->APB2ENR |= (1<<0);			//Initialize AFIO
	portInit(GPIOC);						//Initialize PORTC
	GPIOC->CRH |= 0x00300000;		//PC13 OUTPUT
	AFIO->EXTICR1 &= ~(1<<0);		//Configure the AFIO_EXTICRx for the mapping of external interrupt/event lines onto GPIOs
	
	/*===========================================================================================================*/
	//	To configure the 20 lines as interrupt sources, use the following procedure:
	EXTI->IMR  |= (1<<0);				//	� Configure the mask bits of the 20 Interrupt lines (EXTI_IMR)
	EXTI->FTSR |= (1<<0);				//	� Configure the Trigger Selection bits of the Interrupt lines (EXTI_RTSR and EXTI_FTSR)
	EXTI->RTSR |= (1<<0);
	NVIC->ISER[0] |= (1<<6);		//	� Configure the enable and mask bits that control the NVIC IRQ channel mapped to the
															//	  External Interrupt Controller (EXTI) so that an interrupt coming from one of the 20 lines
															//	  can be correctly acknowledged.
	/*===========================================================================================================*/

	GPIOC->ODR |= (1<<13);
	//Loop
	while (1)
	{
		
	}
}

void EXTI0_IRQHandler()
{
	if(EXTI->PR&0x01){
		EXTI->PR =0x01;						//This bit is set when the selected edge event arrives on the external interrupt line. This bit is cleared by writing a �1� into the bit.
		GPIOC->ODR ^= (1<<13);
	}
}