#include <stm32f10x.h>
#define GPIOB_CLOCKEN      (1<<3)


//ODR
#define GPIO_Pin_0              0x0001
#define GPIO_Pin_1              0x0002
#define GPIO_Pin_2              0x0004
#define GPIO_Pin_3              0x0008
#define GPIO_Pin_4              0x0010
#define GPIO_Pin_5              0x0020
#define GPIO_Pin_6              0x0040
#define GPIO_Pin_7              0x0080
//CRL
#define GPIO_Pin_0_OUTPUT 			(3<<0)
#define GPIO_Pin_1_OUTPUT 			(3<<4)
#define GPIO_Pin_2_OUTPUT 			(3<<8)
#define GPIO_Pin_3_OUTPUT 			(3<<12)
#define GPIO_Pin_4_OUTPUT 			(3<<16)
#define GPIO_Pin_5_OUTPUT 			(3<<20)
#define GPIO_Pin_6_OUTPUT 			(3<<24)
#define GPIO_Pin_7_OUTPUT 			(3<<28)



// Pin definition
//#define LCD16X2_PIN_RW             GPIO_Pin_13

#define     LCM_OUT               GPIOB->ODR
#define     LCM_PIN_RS            GPIO_Pin_0          // PB0
#define     LCM_PIN_EN            GPIO_Pin_1          // PB1
#define     LCM_PIN_D7            GPIO_Pin_7          // PB7
#define     LCM_PIN_D6            GPIO_Pin_6          // PB6
#define     LCM_PIN_D5            GPIO_Pin_5          // PB5
#define     LCM_PIN_D4            GPIO_Pin_4          // PB4
#define     LCM_PIN_MASK  ((LCM_PIN_RS | LCM_PIN_EN | LCM_PIN_D7 | LCM_PIN_D6 | LCM_PIN_D5 | LCM_PIN_D4))

void delay(int a);



void PulseLCD();

void SendByte(char ByteToSend, int IsData);


void Cursor(char Row, char Col);


void ClearLCDScreen();



void InitializeLCD(void);

void PrintStr(char *Text);
