#include "stm32f10x.h"

#include "lcd.h"

#define DELAY_UNIT_MS 30
#define GPIOA_CLOCKEN      (1<<2)

#include "functions.h"


void SystemInit(){}
 
int main(void)
{
		RCC->APB2ENR |= GPIOA_CLOCKEN;
		GPIOA->CRL = 0x00000004;
    InitializeLCD(); //Initialize LCD
    ClearLCDScreen(); //CLEAR LCD
 
    while(1)
    {
			if(GPIOA->IDR & 0x0001) //check if switch is pressed
			{
					delay_ms(DELAY_UNIT_MS);	//wait for debouncing
					Cursor(0,2); //Set Cursor to the First line second Character
					PrintStr("Switch"); //Print String 
					Cursor(1,7);  //second line
					PrintStr("ON ");
			}
			else
			{
					Cursor(0,2); //Set Cursor to the First line second Character
					PrintStr("Switch"); //Print String 
					Cursor(1,7);  //second line
					PrintStr("OFF");
			}

 
    }
}
