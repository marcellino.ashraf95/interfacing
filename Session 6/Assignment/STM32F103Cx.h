typedef unsigned long int uint_8;

#define GPIO_PIN0				0
#define GPIO_PIN1				1
#define GPIO_PIN2				2
#define GPIO_PIN3				3
#define GPIO_PIN4				4
#define GPIO_PIN5				5
#define GPIO_PIN6				6
#define GPIO_PIN7				7
#define GPIO_PIN8				8
#define GPIO_PIN9				9
#define GPIO_PIN10			10
#define GPIO_PIN11			11
#define GPIO_PIN12			12
#define GPIO_PIN13			13
#define GPIO_PIN14			14
#define GPIO_PIN15			15

#define GPIO_PIN0_OUT			(3<<GPIO_PIN0)
#define GPIO_PIN1_OUT			(3<<GPIO_PIN1)
#define GPIO_PIN2_OUT			(3<<GPIO_PIN2)
#define GPIO_PIN3_OUT			(3<<GPIO_PIN3)
#define GPIO_PIN4_OUT			(3<<GPIO_PIN4)
#define GPIO_PIN5_OUT			(3<<GPIO_PIN5)
#define GPIO_PIN6_OUT			(3<<GPIO_PIN6)
#define GPIO_PIN7_OUT			(3<<GPIO_PIN7)
#define GPIO_PIN8_OUT			(3<<GPIO_PIN8)
#define GPIO_PIN9_OUT			(3<<GPIO_PIN9)
#define GPIO_PIN10_OUT		(3<<GPIO_PIN10)
#define GPIO_PIN11_OUT		(3<<GPIO_PIN11)
#define GPIO_PIN12_OUT		(3<<GPIO_PIN12)
#define GPIO_PIN13_OUT		(3<<GPIO_PIN13)
#define GPIO_PIN14_OUT		(3<<GPIO_PIN14)
#define GPIO_PIN15_OUT		(3<<GPIO_PIN15)

#define GPIO_PIN0_IN			(4<<GPIO_PIN0)
#define GPIO_PIN1_IN			(4<<GPIO_PIN1)
#define GPIO_PIN2_IN			(4<<GPIO_PIN2)
#define GPIO_PIN3_IN			(4<<GPIO_PIN3)
#define GPIO_PIN4_IN			(4<<GPIO_PIN4)
#define GPIO_PIN5_IN			(4<<GPIO_PIN5)
#define GPIO_PIN6_IN			(4<<GPIO_PIN6)
#define GPIO_PIN7_IN			(4<<GPIO_PIN7)
#define GPIO_PIN8_IN			(4<<GPIO_PIN8)
#define GPIO_PIN9_IN			(4<<GPIO_PIN9)
#define GPIO_PIN10_IN			(4<<GPIO_PIN10)
#define GPIO_PIN11_IN			(4<<GPIO_PIN11)
#define GPIO_PIN12_IN			(4<<GPIO_PIN12)
#define GPIO_PIN13_IN			(4<<GPIO_PIN13)
#define GPIO_PIN14_IN			(4<<GPIO_PIN14)
#define GPIO_PIN15_IN			(4<<GPIO_PIN15)

typedef struct
{
	uint_8 CRL;
	uint_8 CRH;
	uint_8 IDR;
	uint_8 ODR;
	uint_8 BSRR;
	uint_8 BRR;
	uint_8 LCKR;
}GPIO_TYPEDEF;

#define APB2_BASE			0x40010000
#define GPIOA_BASE		(APB2_BASE + 0x0800)
#define GPIOB_BASE		(APB2_BASE + 0x0C00)
#define GPIOC_BASE		(APB2_BASE + 0x1000)
#define GPIOD_BASE		(APB2_BASE + 0x1400)
#define GPIOE_BASE		(APB2_BASE + 0x1800)
#define GPIOF_BASE		(APB2_BASE + 0x1C00)
#define GPIOG_BASE		(APB2_BASE + 0x2000)

#define GPIOA					((GPIO_TYPEDEF *)GPIOA_BASE)
#define GPIOB					((GPIO_TYPEDEF *)GPIOB_BASE)
#define GPIOC					((GPIO_TYPEDEF *)GPIOC_BASE)
#define GPIOD					((GPIO_TYPEDEF *)GPIOD_BASE)
#define GPIOE					((GPIO_TYPEDEF *)GPIOE_BASE)
#define GPIOF					((GPIO_TYPEDEF *)GPIOF_BASE)
#define GPIOG					((GPIO_TYPEDEF *)GPIOG_BASE)

typedef struct
{
	uint_8 CR;
	uint_8 CFGR;
	uint_8 CIR;
	uint_8 APB2RSTR;
	uint_8 APB1RSTR;
	uint_8 AHBENR;
	uint_8 APB2ENR;
	uint_8 APB1ENR;
	uint_8 BDCR;
	uint_8 CSR;
}RCC_TYPEDEF;

#define RCC_BASE 		  0x40021000

#define RCC						((RCC_TYPEDEF *)RCC_BASE)

char portInit (GPIO_TYPEDEF * GPIO);
void portOut (GPIO_TYPEDEF * GPIO, long value);
void pinOut (GPIO_TYPEDEF * GPIO, unsigned char pinValue);
