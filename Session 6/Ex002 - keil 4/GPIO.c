#include "GPIO.h"

void
CLK_EN
(GPIO_TYPEDEF * GPIO)
{
	if (GPIO == GPIOA)
	{
		RCC->APB2ENR |= (1<<2);			
	}
	else
	{
		if (GPIO == GPIOB)
		{
			RCC->APB2ENR |= (1<<3);			
		}
		else
		{
			if (GPIO == GPIOC)
			{
				RCC->APB2ENR |= (1<<4);			
			}
			else
			{			
				if (GPIO == GPIOD)
				{
					RCC->APB2ENR |= (1<<5);			
				}
				else
				{
					if (GPIO == GPIOE)
					{
						RCC->APB2ENR |= (1<<6);
					}
					else
					{
						if (GPIO == GPIOF)
						{
							RCC->APB2ENR |= (1<<7);
						}
						else
						{
							if (GPIO == GPIOG)
							{
								RCC->APB2ENR |= (1<<8);
							}
						}
					}
				}
			}
		}
	}
}

unsigned long int
PORT_IN
(GPIO_TYPEDEF *GPIO)
{
	return GPIO->IDR;
}
