/*
Author: Marcellino Ashraf
Date: 13/4/2019
*/

#include "GPIO.h"

void SystemInit(){}
	
int  main()
{
	unsigned long int counter;
	//setup
	RCC->APB2ENR |= (1<<2);	//CLK to GPIO A
	GPIOA->CRL |= (3<<0);		//PA0 as output	
	

	
	//loop
	while (1)
	{
		GPIOA->ODR |= (1<<0);														//LED ON
		for(counter = 0; counter < 100000; counter++);		//DEELAY
		GPIOA->ODR &= ~(1<<0);													//LED OFF
		for(counter = 0; counter < 100000; counter++);		//DEELAY
	}
}
