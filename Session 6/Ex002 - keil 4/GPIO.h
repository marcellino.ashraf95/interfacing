//#define ADDR(x)	(*(volatile unsigned long *) x)
	
#define PERIPH_APB2	0x40010000
#define GPIOA_BASE  (PERIPH_APB2 + 0x0800)
#define GPIOB_BASE  (PERIPH_APB2 + 0x0C00)
#define GPIOC_BASE  (PERIPH_APB2 + 0x1000)
#define GPIOD_BASE  (PERIPH_APB2 + 0x1400)
#define GPIOE_BASE  (PERIPH_APB2 + 0x1800)
#define GPIOF_BASE  (PERIPH_APB2 + 0x1C00)
#define GPIOG_BASE  (PERIPH_APB2 + 0x2000)

typedef struct
{
	volatile unsigned long CRL;
	volatile unsigned long CRH;
	volatile unsigned long IDR;
	volatile unsigned long ODR;
	volatile unsigned long BSRR;
	volatile unsigned long BRR;
	volatile unsigned long LCKR;
}GPIO_TYPEDEF;

#define GPIOA			((GPIO_TYPEDEF *)GPIOA_BASE)
#define GPIOB			((GPIO_TYPEDEF *)GPIOB_BASE)
#define GPIOC			((GPIO_TYPEDEF *)GPIOC_BASE)
#define GPIOD			((GPIO_TYPEDEF *)GPIOD_BASE)
#define GPIOE			((GPIO_TYPEDEF *)GPIOE_BASE)
#define GPIOF			((GPIO_TYPEDEF *)GPIOF_BASE)
#define GPIOG			((GPIO_TYPEDEF *)GPIOG_BASE)

#define RCC_BASE		0x40021000

typedef struct
{
	volatile unsigned long CR;
	volatile unsigned long CFGR;
	volatile unsigned long CIR;
	volatile unsigned long APB2RSTR;
	volatile unsigned long APB1RSTR;
	volatile unsigned long AHBENR;
	volatile unsigned long APB2ENR;
	volatile unsigned long APB1ENR;
	volatile unsigned long BDCR;
	volatile unsigned long CSR;
}RCC_TYPEDEF;

#define RCC			((RCC_TYPEDEF *)RCC_BASE)

