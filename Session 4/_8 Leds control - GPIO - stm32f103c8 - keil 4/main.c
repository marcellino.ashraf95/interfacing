/*
Author: Marcellino Ashraf
Description:
Make a program to control 8 leds from a button,
they should emit light one after another from led_0 to led_8,
then repeat it once again,
then stop and wait for another button press.
*/

#include <stm32f10x.h>
#include <stdio.h>
#include "functions.h"

#define DELAY_UNIT_MS 30
#define loops 2

void SystemInit(){}

int main()
{
	//setup
	RCC->APB2ENR |= (1<<4);	//CLK to PORTC (output)
	RCC->APB2ENR |= (1<<2);	//CLK to PORTA (input)
	
	GPIOA->CRH = 0x30000000;	//Pin A15 input
	GPIOC->CRH = 0x33333333;	//ALL 8 PINS AS OUTPUT
	
	int led_order = 0,							//counter to loop on leds
			loops_counter = 0;					//counter to loop how many times
	
	
	
	//loop
	while(1)
	{
		if(GPIOA->ODR & 0x8000) //check if switch is pressed
		{
			delay_ms(DELAY_UNIT_MS);	//wait for debouncing
			if(GPIOA->ODR & 0x8000)		//if still pressed
			{
				while (GPIOA->ODR & 0x8000);	//wait till unpressed
				delay_ms(DELAY_UNIT_MS);			//wait for debouncing
				
				for (loops_counter = 0; loops_counter < loops; loops_counter++) //how many loops to repeat
				{
					for (led_order = 0; led_order < 8; led_order++)	//loop on all leds
					{
						GPIOC->ODR = (1<<led_order);	
						delay_ms(DELAY_UNIT_MS*3);	//delay to see leds
					}
				}
			}
		}	
	}
	return 0;
}
