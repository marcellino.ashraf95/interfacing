#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	int counter = 0, i = 0;

	//Setup
	RCC->APB2ENR |= (1<<4);	//CLK to PORTC (input)
	RCC->APB2ENR |= (1<<2);	//CLK to PORTA (output)
	
	GPIOA->CRH = 0x40000000;	//Pin A15 input
	GPIOC->CRH = 0x33300000;	//Pins C13, C14 & C15 output
	
	
	//Loop
	while (1)
	{
		for(i=0; i < 100000; i++);
		if (GPIOA->IDR & 0x8000)
		{
			if (counter > 2)
				counter = 1;
			else
				counter++;
			switch (counter)
			{
				case 1:
					GPIOC->ODR = 0;
					GPIOC->ODR |= (1<<13);
					for(i=0; i < 100000; i++);
					break;
				case 2:
					GPIOC->ODR = 0;
					GPIOC->ODR |= (1<<14);
					for(i=0; i < 100000; i++);
					break;
				case 3:
					GPIOC->ODR = 0;
					GPIOC->ODR |= (1<<15);
					for(i=0; i < 100000; i++);
					break;
				default:
					GPIOC->ODR = 0;
					for(i=0; i < 100000; i++);
					break;

			}
		}
	}
	
	
	return 0;
}
