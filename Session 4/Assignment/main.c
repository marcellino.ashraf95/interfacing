#include <stm32f10x.h>
#include <stdio.h>

#define DELAY_UNIT 1000
#define PC0 (1<<0)
#define PC1 (1<<1)
#define PC2 (1<<2)
#define PC3 (1<<3)
#define PC4 (1<<4)
#define PC5 (1<<5)
#define PC6 (1<<6)
#define PC7 (1<<7)
#define loops 2

int main()
{
	//setup
	RCC->APB2ENR |= (1<<4);	//CLK to PORTC (input)
	RCC->APB2ENR |= (1<<2);	//CLK to PORTA (output)
	
	GPIOA->CRH = 0x30000000;	//Pin A15 input
	GPIOC->CRH = 0x33333333;	//aLL 8 PINS AS OUTPUT
	
	unsigned long int counter = 0;
	int led_order = 0, loops_counter = 0;
	
	GPIOA->ODR = 0x8000;
	
	
	//loop
	while(1)
	{
		if(GPIOA->ODR & 0x8000)
		{
			for (counter = 0; counter < DELAY_UNIT*10; counter++);
			if(GPIOA->ODR & 0x8000)
			{
				while (GPIOA->ODR & 0x8000);
				for (counter = 0; counter < DELAY_UNIT*10; counter++);
				
				for (loops_counter = 0; loops_counter < loops; loops_counter++)
				{
					for (led_order; led_order < 8; led_order++)
					{
						GPIOC->ODR = (1<<led_order);
						for (counter = 0; counter < DELAY_UNIT*100; counter++);
					}
				}
			}
		}	
	}
	return 0;
}
