#include "STM32F103Cx.h"

char
portInit
(GPIO_TYPEDEF * GPIO)
{
	char check = 0;
	if (GPIO == GPIOA)
		RCC->APB2ENR |= (1<<2);
	else
		if (GPIO == GPIOB)
			RCC->APB2ENR |= (1<<3);
		else
			if (GPIO == GPIOC)
				RCC->APB2ENR |= (1<<4);
			else
				if (GPIO == GPIOD)
					RCC->APB2ENR |= (1<<5);
				else
					if (GPIO == GPIOE)
						RCC->APB2ENR |= (1<<6);
					else
						if (GPIO == GPIOF)
							RCC->APB2ENR |= (1<<7);
						else
							if (GPIO == GPIOG)
								RCC->APB2ENR |= (1<<8);
	check = 1;
	if (check == 1)
		return 1;
	else
		return 0;
}

void
portOut
(GPIO_TYPEDEF * GPIO,
long int value)
{
	GPIO->ODR |= value;
}

void
pinOut
(GPIO_TYPEDEF * GPIO,
 unsigned char pinValue)
{
	GPIO->ODR |= pinValue;
}	 
