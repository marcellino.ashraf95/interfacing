#include "STM32F103Cx.h"

void SystemInit(){};

int main()
{
	uint_32 counter = 0;
	uint_8 data = 0;
	//Setup
	RCC->APB2ENR |=  (1<<14)|(1<<2);						//Enable CLK to USART1 (1<<14), GPIOA (1<<2) and AFIO (1<<0)
	USART1->CR1  |=  (1<<13);													//Enable  the USART by writing  the UE bit in USART_CR1 register to 1.
	USART1->CR1  &= ~(1<<12);													//Program the M bit in USART_CR1 to define the word length. (8 BITS)
	USART1->CR2  &= ~(3<<12);													//Program the number of stop bits in USART_CR2. (1 STOP)
	USART1->CR3  &= ~(1<<7);													//Select DMA enable (DMAT) in USART_CR3 if Multi buffer Communication is to take place. Configure the DMA register as explained in multibuffer communication.
	USART1->BRR   =   8000000/115200;									//Select the desired baud rate using the USART_BRR register.
	USART1->CR1  |=  (1<<2);													//Set the RE bit in USART_CR1 to send an idle frame as first transmission.
																	//After writing the last data into the USART_DR register, wait until TC=1. This indicates that the transmission of the last frame is complete. This is required for instance when the USART is disabled or enters the Halt mode to avoid corrupting the last transmission.
	
	GPIOA->CRH |= (0xB<<4);			//PA9 as alternate function output push-pull
	GPIOA->CRH |= (4<<8);				//PA10 as floating input
	GPIOA->CRL |= (3<<1);				//PA1 as output input
	
	//Loop
	while(1)
	{	
		while(!(USART1->SR & (1<<5)));
			data = USART1->DR & 0x01FF;														//Read the data to receive in the USART_DR register
			if (data == 'M')
			{
				GPIOA->ODR =0x00ff;
				for(counter = 0; counter < 100000; counter++);				
			}

			else
				GPIOA->ODR &=~(1<<1);		
	}	
}
