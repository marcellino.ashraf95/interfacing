#include "STM32F103Cx.h"

void SystemInit(){};
	
int data = 0;
uint_32 delay;
	
int main()
{
	//Setup
	NVIC->ISER[1] = 1<<5; //enable USART1 interrupts
	USART1_Init();
	RCC->APB2ENR |= (1<<4);
	GPIOC->CRH |= 0x00300000;			//PC13 output
	GPIOC->ODR |= (1<<13);
	//Loop
	while (1)
	{
		if (data == 'm')
		{	
			GPIOC->ODR ^= ~(1<<13);
			for(delay = 0; delay<100000; delay++);
		}
	}
}


void USART1_IRQHandler()
{
	if (USART1->SR & (1<<5))
	{
		data = USART1->DR;
	}
}
